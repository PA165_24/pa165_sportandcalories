/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.entity;

/**
 *
 * @author Peter Karlubik
 */
public enum Sex {
    MALE, FEMALE, UNSPECIFIED
}
