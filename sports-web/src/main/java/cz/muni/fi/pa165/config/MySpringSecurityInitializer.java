/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * It will load the springSecurityFilterChain automatically
 * @author Tomas Balogh
 */
public class MySpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {
    // do nothing
}
